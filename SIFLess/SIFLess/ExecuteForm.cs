﻿using SIFLess.Events;
using SIFLess.Helpers;
using System;
using System.Diagnostics;
using System.IO;
using System.Management.Automation;
using System.Text;
using System.Windows.Forms;

namespace SIFLess
{
    public partial class ExecuteForm : Form
    {
        private string _fileName;
        private DateTime _start;

        private delegate void ProcessCompletedCallaback(ProcessCompletedEventArgs processCompleted);

        public ExecuteForm()
        {
            InitializeComponent();
        }


        public void AppendTextBox(string value)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(AppendTextBox), value);
                return;
            }
            textBox1.AppendText(value + "\r\n");
            Application.DoEvents();
        }

        public void Run(string fileName)
        {
            _start = DateTime.Now;
            _fileName = fileName; // store the file name for the backgroun thread to use.
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Processor processor = new Processor(_fileName);
            processor.ProcessUpdated += Processor_ProcessUpdated;
            processor.ProcessCompleted += Processor_ProcessCompleted;

            processor.Execute();
        }

        private void Processor_ProcessCompleted(ProcessCompletedEventArgs processCompleted)
        {
            if (InvokeRequired)
            {
                Invoke(new ProcessCompletedCallaback(this.Close), new object[] { processCompleted });
            }
            else
            {
                Close(processCompleted);
            }
        }

        

        private void Processor_ProcessUpdated(ProcessUpdatedEventArgs processUpdated)
        {
            string loggedTime = $"{processUpdated.LoggedDateTime.ToShortDateString()} {processUpdated.LoggedDateTime.ToString("HH:mm:ss.fff")}";
            AppendTextBox($"{loggedTime} : {processUpdated.Message}");
        }



        private void Close(ProcessCompletedEventArgs processCompleted)
        {
            TimeSpan elapsed = DateTime.Now.Subtract(_start);
            if (processCompleted.Success)
            {
                MessageBox.Show($"Done executing in {elapsed.ToReadableString()} " + _fileName);
            }
            else
            {
                MessageBox.Show("Something went wrong. Check the log files", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
