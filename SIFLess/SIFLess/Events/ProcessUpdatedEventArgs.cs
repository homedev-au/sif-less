﻿using System;

namespace SIFLess.Events
{
    public class ProcessUpdatedEventArgs : EventArgs
    {
        public ProcessUpdatedEventArgs(string msg)
        {
            this.Message = msg;
            this.LoggedDateTime = DateTime.Now;
        }

        public DateTime LoggedDateTime { get; set; }
        public string Message { get; set; }
    }
}
