﻿using System;

namespace SIFLess.Events
{
    public class ProcessCompletedEventArgs : EventArgs
    {
        public ProcessCompletedEventArgs(bool success, int exitCode)
        {
            Success = success;
            ExitCode = exitCode;
        }

        public bool Success { get; private set; }
        public int ExitCode { get; private set; }
    }
}
