﻿using SIFLess.Events;
using System;
using System.Diagnostics;
using System.Management.Automation;
using System.Timers;

namespace SIFLess.Helpers
{
    /// <summary>
    /// Created by: John Crawford - http://www.homedev.com.au
    /// Created date: 17-Jan-2018
    /// This is a class to manage the processing of the powershell script on the background thread to improve performance.
    /// Previously the Execute code was running in the ExecuteForm, and was running a while loop that was causing the application
    /// to run at 100% CPU for no reason. This implementation avoids the while loop and improves performance
    /// Based upon this article of mine
    /// http://ntsblog.homedev.com.au/index.php/2012/03/30/background-worker-thread-code-sample-event-handlers-cross-thread-invoke/
    /// </summary>
    public class Processor
    {
        private  string _filename;
        private Timer _timer;
        private Process _process;

        public delegate void ProcessUpdatedEvent(ProcessUpdatedEventArgs processUpdated);
        public event ProcessUpdatedEvent ProcessUpdated;

        public delegate void ProcessCompletedEvent(ProcessCompletedEventArgs processCompleted);
        public event ProcessCompletedEvent ProcessCompleted;

        public Processor(string filename)
        {
            _filename = filename;
        }

        public void Execute()
        {
            DateTime start = DateTime.Now;

            // create and start timer
            _timer = new Timer(2000);
            _timer.Elapsed += timer_Elapsed;
            _timer.Start();

            //We're gonna need to grant some permissions...just for this process
            using (var psInst = PowerShell.Create())
            {
                psInst.AddScript("Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted");
                psInst.Invoke();
            }

            var pInfo = new ProcessStartInfo("powershell.exe");
            pInfo.WorkingDirectory = Environment.CurrentDirectory;
            pInfo.UseShellExecute = false;
            pInfo.RedirectStandardOutput = true;
            pInfo.RedirectStandardError = true;
            pInfo.CreateNoWindow = true;
            pInfo.Arguments = $"-file \"{_filename}\""; // JC: quoted as would crash if config path contained a space

            _process = new Process();
            _process.StartInfo = pInfo;
            _process.OutputDataReceived += (senderOD, args) => RaiseProcessUpdatedEvent(args.Data);
            _process.ErrorDataReceived += (senderOD, args) => RaiseProcessUpdatedEvent(args.Data);
            _process.Start();

            _process.BeginOutputReadLine();
            _process.BeginErrorReadLine();

        }

        /// <summary>
        /// Using timer to check for the process exit as the exit event was not firing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_process != null && _process.HasExited)
            {
                _timer.Stop();
                RaiseCompletedEvent();
            }
        }

        private  void RaiseProcessUpdatedEvent(string msg)
        {
            if (ProcessUpdated != null)
            {
                ProcessUpdatedEventArgs args = new ProcessUpdatedEventArgs(msg);
                ProcessUpdated(args);
            }
        }

        private void RaiseCompletedEvent()
        {
            if(ProcessCompleted != null)
            {
                bool success = _process.ExitCode == 0;
                ProcessCompletedEventArgs args = new ProcessCompletedEventArgs(success, _process.ExitCode);
                ProcessCompleted(args);
            }
        }
    }
}
